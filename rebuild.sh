#!/bin/bash
set -e

vagrant halt -f
vagrant destroy -f
bash login.sh
