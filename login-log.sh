#!/bin/bash
set -e

T_BEGIN=$(date +'%s')

vagrant up

T_FINISH=$(date +'%s')
T_ELAPSED=$(($T_FINISH-$T_BEGIN))
T_MAX="120" # 120 seconds (2 minutes)

echo '--------------------'
echo 'Time for vagrant up:'
echo "$(($T_ELAPSED / 60)) minutes and $(($T_ELAPSED % 60)) seconds"

# NOTE: For some odd reason, the /home/vagrant/shared directory is owned by the root user
# instead of the regular vagrant user at the first bootup.
# Rebooting resolves this issue.
if [ "$T_ELAPSED" -gt "$T_MAX" ]
then
  # This applies if the provisioning process was executed,
  # which increases the time needed to execute vagrant up.
  echo 'Rebooting Vagrant box'
  vagrant halt -f
  vagrant up
fi
