#!/bin/bash
set -e

DATE=`date -u +%Y%m%d-%H%M%S-%3N`

mkdir -p log
bash login-log.sh $APP_NAME 2>&1 | tee log/login-$DATE.txt

echo '-----------'
echo 'vagrant ssh'
vagrant ssh
