#!/bin/bash
set -e

echo '----------------'
echo 'vagrant box list'
echo '----------------'
vagrant box list
echo ''
echo 'Enter the name of the Vagrant base box you wish to remove:'
read BOX_TO_REMOVE

vagrant halt -f
vagrant destroy -f
vagrant box remove -f $BOX_TO_REMOVE
