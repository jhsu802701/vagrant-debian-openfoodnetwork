# Vagrant Debian - Open Food Network

## Purpose
This repository is designed for configuring a Vagrant box for setting up a development environment for a specific project.  If you have not already done so, please customize the Vagrantfile and the provisioning scripts for that project.  Edit this README.md file to specify which project this Vagrant box repository is for.

## Starting and Stopping a Vagrant Box
* The scripts used for starting a new Vagrant box are described in the README-scripts.md file.  As soon as the Vagrant box has been started, you will automatically be given ssh access to it.  (The "vagrant" in the prompt will indicate this.).
* If you wish to gain ssh access through additional shell tabs or windows, just start a new shell/window and enter the command `vagrant ssh` from this directory to enter the Vagrant box.  This allows you to dedicate one window to running a server and another window to executing other commands within the Vagrant environment.
* To stop the Vagrant box, enter the command `vagrant halt -f`.
* To destroy the Vagrant box, enter the command `vagrant destroy -f`.

## Usage
* When you ssh into a Vagrant box, you will be in the /home/vagrant directory as the vagrant user.
* Do your work in the /home/vagrant/shared directory within the Vagrant box.  This directory maps to the shared directory in this repository on your host setup.  This allows you to use the tools on your host system to add, delete, and edit files within the Vagrant box.

## Things to Do Upon Starting this Repository
* Start a new GitLab, BitBucket, or GitHub repository.
* Follow the instructions for pushing this repository.

## Things to customize
* Vagrantfile
* Provisioning scripts

## Updating this Repository
When the time comes, upgrade the Debian version.  Update the "config.vm.box" parameter in the Vagrantfile.
