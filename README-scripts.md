# Bash Scripts

There are several ways to start a new Vagrant box:

* login.sh: Run the command `bash login.sh` to log into the Vagrant box.
* reboot.sh: Run the command `bash reboot.sh` to reboot and log into this Vagrant box.
* rebuild.sh: Run the command `bash rebuild.sh` to destroy and rebuild the Vagrant box.
* update.sh: Run the command `bash update.sh` to update your local version of the base box specified in the Vagrantfile.  If it's different from the current version of the base box on your local machine, Vagrant will download the new version of the base box and provision it.
* remove.sh: Run the command `bash remove.sh` to fully delete the Vagrant box and the base box.
